# ZeroClip

ZeroClip is simple Windows Clipboard manipulator and clipboard history viewer for text type of clips. Listed items can be used to substitute currently stored clipboard item. First item on list represents a text item currently on the Windows clipboard.

You can download zip of installation media from address: https://bitbucket.org/MarkoMS/zeroclip/downloads/ZeroClip.zip. Installation media is currently ClickOnce type so it will be installed automatically to default folder path. If you need to update this type of media you need to remove the old one first and then install current version.

Copyright @ 2023, Marko Suomalainen