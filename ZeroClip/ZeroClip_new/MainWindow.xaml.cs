﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.IO;
using System.Windows.Threading;
using ZeroClip_new.Classes;

namespace ZeroClip_new
{
    /// <summary>
    /// Clipboard application with visual item view
    /// </summary>
    public partial class MainWindow : Window
    {
        private UserClipHistory _history = new UserClipHistory();
        private static string _localPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "ZeroClip");
        private static string _appDataLocal = System.IO.Path.Combine(_localPath, "History.xml");
        private DispatcherTimer _timer = new DispatcherTimer();
        private XmlReadWrite _readWrite = new XmlReadWrite();
        public PreferencesWindow _prefWindow;

        public MainWindow()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CaughtUnhandledEvent); // Set crash reporting before functionality construction.

            InitializeComponent();
            LoadHistory();
            SetInitialWindowSettings();
            SetTimer();
            _readWrite.Error += WriteDebug;
        }

        #region Dependency Properties

        public List<HistoryItemControl> HistoryList
        {
            get { return (List<HistoryItemControl>)GetValue(HistoryListProperty); }
            set { SetValue(HistoryListProperty, value); }
        }

        // Depedency property of HistoryList
        public static readonly DependencyProperty HistoryListProperty = DependencyProperty.Register("HistoryList", typeof(List<HistoryItemControl>), typeof(MainWindow));

        #endregion

        #region Common methods

        /// <summary>
        /// Initializes timer and it's tick event.
        /// </summary>
        private void SetTimer()
        {
            _timer.Tick += CheckClipBoard;
            UpdateTimerInterval();
        }

        /// <summary>
        /// Update timer interval and restart the state of the timer
        /// </summary>
        private void UpdateTimerInterval()
        {
            _timer.Stop();
            _timer.Interval = new TimeSpan(0, 0, 0, 0, _history.UpdateInterval);
            _timer.Start();
        }

        /// <summary>
        /// Set properties that need activation
        /// </summary>
        private void SetInitialWindowSettings()
        {
            SetAlwaysOnTop(_history.AlwaysOnTop, EventArgs.Empty);
        }

        /// <summary>
        /// Add events to new history items
        /// </summary>
        /// <param name="item">History item</param>
        private void SetHistoryItemEvents(HistoryItemControl item)
        {
            item.HistoryClicked += SetAsFirstItem;
            item.RemoveHistoryItem += RemoveHistoryItemFromList;

        }

        /// <summary>
        /// Create local data folder if it doesn't exist (.\appdata\local\ZeroClip)
        /// </summary>
        private void LocalDataFolderExists()
        {
            if (!Directory.Exists(_localPath))
                Directory.CreateDirectory(_localPath);
        }

        #endregion

        #region Save
        /// <summary>
        /// Save Clipboard history to a file
        /// </summary>
        private void Save()
        {
            LocalDataFolderExists();
            _readWrite.Write<UserClipHistory>(_history, _appDataLocal);
        }

        #endregion

        #region Load previously saved data

        /// <summary>
        /// Load history from file
        /// </summary>
        private void LoadHistory()
        {
            string clipBoardContent = Clipboard.GetText();
            int historyItemCount = 0;
            _history.Clear();

            if (File.Exists(_appDataLocal))
            {
                try
                {
                    _history = _readWrite.Read<UserClipHistory>(_appDataLocal);
                }
                catch (Exception e)
                {
                    WriteDebug(e);
                }
                finally
                {
                    historyItemCount = _history.GetHistory().Count;
                }
            }

            if (historyItemCount == 0 && !String.IsNullOrEmpty(clipBoardContent)) //handling empty history.xml file but clipboard might have previous content
                _history.Add(clipBoardContent);
            
            UpdateDataOnWindow();
        }

        #endregion

        #region Textbox list manipulation cycle

        /// <summary>
        /// Create UI elements from local history list and update data on UI
        /// </summary>
        private void UpdateDataOnWindow()
        {
            List<HistoryItemControl> currentHistory = new List<HistoryItemControl>();
            int historyCount = _history.ItemCount;

            for (int i = 1; i <= historyCount; i++)
            {
                HistoryItemControl historyItem = new HistoryItemControl
                {
                    ID = i,
                    Item = _history.GetElement(historyCount - i), //history item list has latest item on last index of the List -> index requires reversal for UI elements
                };

                SetHistoryItemEvents(historyItem);
                currentHistory.Add(historyItem);
            }

            HistoryList = currentHistory; //update depedency properties
            Clipboard.Clear(); //Web forums indicate that clearing Clipboard first reduces errors about it being write locked or in use by other apps. Also if _history object is already empty it doesn't require 'else' statement on below.
                               
            if (historyCount > 0)
                Clipboard.SetText(_history.GetElement(_history.ItemCount - 1));

        }

        #endregion

        #region HistoryItemList manipulation

        /// <summary>
        /// Remove clicked history item from list
        /// </summary>
        /// <param name="sender">UserControl element of clicked sub element</param>
        /// <param name="e">Event args</param>
        private void RemoveHistoryItemFromList(object sender, EventArgs e)
        {
            HistoryItemControl itemToRemove = (HistoryItemControl)sender;
            _history.Remove(itemToRemove.Item);
            itemToRemove.RemoveHistoryItem -= RemoveHistoryItemFromList;

            UpdateDataOnWindow();
        }

        /// <summary>
        /// Set Clipboard content as copied text value
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event args</param>
        private void SetAsFirstItem(object sender, EventArgs e)
        {
            HistoryItemControl item = (HistoryItemControl)sender;
            _history.Add(item.Item);

            UpdateDataOnWindow();
        }

        #endregion

        #region Clipboard state check

        /// <summary>
        /// Checks Windows clipboard content. Will update history and UI if first history item is not set as clipboard's value
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event arguments</param>
        private void CheckClipBoard(object sender, EventArgs e)
        {
            if (Clipboard.GetDataObject() != null && !String.IsNullOrEmpty(Clipboard.GetText(TextDataFormat.Text)))
            {
                string clipBoardItem = Clipboard.GetText(TextDataFormat.Text);
                string historyContent = "";

                if (_history.ItemCount != 0)
                    historyContent = _history.GetElement(_history.ItemCount-1);
                if (clipBoardItem != historyContent)
                {
                    _history.Add(clipBoardItem);
                    UpdateDataOnWindow();
                }
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Before application closes history will be saved for next use
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Closing event</param>
        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _timer.Stop();
            Save();
        }

        /// <summary>
        /// Exit program
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Routed Event</param>
        private void SubmenuExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Open Preferences popup
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Routed Event</param>
        private void SubmenuPreferences_Click(object sender, RoutedEventArgs e)
        {
            _prefWindow = new PreferencesWindow(_history);
            _prefWindow.AlwaysOnTopChange += SetAlwaysOnTop;
            _prefWindow.CapacityChange += SetListCapacity;
            _prefWindow.TimerIntervalChange += SetTimerInterval;
            _prefWindow.ShowDialog();
        }

        /// <summary>
        /// Set value for AlwaysOnTop
        /// </summary>
        /// <param name="isChecked">True/False</param>
        /// <param name="e">No eventargs</param>
        private void SetAlwaysOnTop(object isChecked, EventArgs e)
        {
            _history.AlwaysOnTop = (bool)isChecked;
            Topmost = (bool)isChecked;
        }

        /// <summary>
        /// Set value for ListCapacity
        /// </summary>
        /// <param name="newCapacity">New capasity value</param>
        /// <param name="e">No eventargs</param>
        private void SetListCapacity(object newCapacity, EventArgs e)
        {
            int newCapacityInt = (int)newCapacity;

            // Trim visible and internal list to match lowered capacity.
            if (newCapacityInt < _history.ItemCount)
            {
                _history.RemoveRangeOldestItems(_history.ListCapacity - newCapacityInt);
                _history.ListCapacity = newCapacityInt;
                UpdateDataOnWindow();
            }
            else // Capacity increase
            {
                _history.ListCapacity = newCapacityInt;
            }
        }

        /// <summary>
        /// Set value for UpdateInterval
        /// </summary>
        /// <param name="intervalTime">Time in ms</param>
        /// <param name="e">No eventargs</param>
        private void SetTimerInterval(object intervalTime, EventArgs e)
        {
            _history.UpdateInterval = (int)intervalTime;
        }

        /// <summary>
        /// Save current clipboard history via Menu item
        /// </summary>
        /// <param name="sender">Save menu button</param>
        /// <param name="e">Routed Event</param>
        private void SubmenuSave_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }

        #endregion

        #region Error logging

        /// <summary>
        /// Write Exception error to a file
        /// </summary>
        /// <param name="ex">Exception</param>
        private void WriteDebug(Exception ex)
        {
            using FileStream stream = File.OpenWrite(System.IO.Path.Combine(_localPath, "debug.txt"));
            string errorText = String.Format("Date of occurance: {1}{0}Source: {2}{0}Message: {3}{0}Target site: {4}{0}StackTrace: {5}{0}",
                Environment.NewLine, DateTime.UtcNow.ToString("dd:MM:yyyy HH:mm:ss"), ex.Source, ex.Message, ex.TargetSite, ex.StackTrace);
            Byte[] errorBytes = new UTF8Encoding(true).GetBytes(errorText);
            stream.Write(errorBytes, 0, errorBytes.Length);
        }

        /// <summary>
        /// Handler for unhandled exception like unexpected app crash
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event args</param>
        private void CaughtUnhandledEvent(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = (Exception)e.ExceptionObject;
            WriteDebug(ex);
        }

        #endregion
    }
}
