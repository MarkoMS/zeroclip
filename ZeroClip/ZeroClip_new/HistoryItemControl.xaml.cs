﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ZeroClip_new
{
    /// <summary>
    /// History item UserControl with data bindings
    /// </summary>
    public partial class HistoryItemControl : UserControl
    {
        public event EventHandler HistoryClicked; //route number element and text content to trigger same event 
        public event EventHandler RemoveHistoryItem; //route history item removal request

        #region Dependency Properties
        public int ID
        {
            get => (int)GetValue(IDProperty);
            set => SetValue(IDProperty, value);
        }
        
        public string Item
        {
            get => (string)GetValue(ItemProperty);
            set => SetValue(ItemProperty, value);
        }

        /// Depedency property of Item
        public static readonly DependencyProperty ItemProperty = DependencyProperty.Register("Item", typeof(string), typeof(HistoryItemControl));
        /// Depedency property of ID
        public static readonly DependencyProperty IDProperty = DependencyProperty.Register("ID", typeof(int), typeof(HistoryItemControl));

        #endregion
        public HistoryItemControl() => InitializeComponent();

        #region Event Handling
        private void ContentTextBlock_MouseUp(object sender, MouseButtonEventArgs e) => HistoryClicked?.Invoke(this, e);

        private void NumberField_MouseUp(object sender, MouseButtonEventArgs e) => HistoryClicked?.Invoke(this, e);

        private void RemoveHistoryItemButton_Click(object sender, RoutedEventArgs e) => RemoveHistoryItem?.Invoke(this, e);

        #endregion
    }
}
