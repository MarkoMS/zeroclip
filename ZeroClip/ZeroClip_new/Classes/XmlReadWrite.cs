﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace ZeroClip_new.Classes
{
    /// <summary>
    /// Object to XML serializer and deserializer
    /// </summary>
    public class XmlReadWrite
    {
        public event ErrorEvent Error; //event to be subsribed by the parent component
        private XmlSerializer _serializer;

        /// <summary>
        /// Initialize class
        /// </summary>
        public XmlReadWrite()
        {

        }

        /// <summary>
        /// Write user given data with types (T) via XML serializer into a file
        /// </summary>
        /// <typeparam name="T">Target type</typeparam>
        /// <param name="data">Class to serialize</param>
        /// <param name="path">File path</param>
        public void Write<T>(object data, string path)
        {
            using TextWriter writer = new StreamWriter(path);
            _serializer = new XmlSerializer(typeof(T));
            try
            {
                _serializer.Serialize(writer, (T)data);
            }
            catch (Exception ex)
            {
                Error?.Invoke(ex);
            }
        }

        /// <summary>
        /// Deserialize objects from a serialized XML file
        /// </summary>
        /// <typeparam name="T">Target class</typeparam>
        /// <param name="filename">File name</param>
        /// <returns>Deserialized object</returns>
        public T Read<T>(string path)
        {
            T series = default; // can't use "new" constructor if using anonymous class

            if (File.Exists(path))
            {
                _serializer = new XmlSerializer(typeof(T));
                using FileStream stream = new FileStream(path, FileMode.Open);
                try
                {
                    series = (T)_serializer.Deserialize(stream);
                }
                catch (Exception ex)
                {
                    Error?.Invoke(ex);
                }
            }
            return series;
        }
    }
}
