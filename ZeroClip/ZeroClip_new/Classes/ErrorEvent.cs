﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZeroClip_new.Classes
{
    public delegate void ErrorEvent(Exception ex);

    interface IErrorEvent
    {
        event ErrorEvent Error;
    }
}
