﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace ZeroClip_new.Classes
{
    /// <summary>
    /// History of text based clips used on the clipboard by the user. Can be used as XML serialization object.
    /// </summary>
    [XmlRoot(ElementName = "History")]
    public class UserClipHistory
    {
        [XmlArray(ElementName = "HistoryItems")]
        [XmlArrayItem(ElementName = "Item")]
        public List<string> _clipHistory = new List<string>(); //capacity could be defined by attribute ListCapacity

        /// <summary>
        /// Max list capacity
        /// </summary>
        [XmlAttribute(AttributeName = "MaxCapacity")]
        public int ListCapacity { get; set; } = 20;

        /// <summary>
        /// Update interval for clipboard check
        /// </summary>
        [XmlAttribute(AttributeName = "UpdateInterval")]
        public int UpdateInterval { get; set; } = 1000; //milliseconds

        /// <summary>
        /// "Always on top" condition
        /// </summary>
        [XmlAttribute(AttributeName = "AlwaysOnTop")]
        public bool AlwaysOnTop { get; set; } = false;

        /// <summary>
        /// Get all items on history list
        /// </summary>
        public List<string> GetHistory() => _clipHistory;

        #region Methods

        /// <summary>
        /// Initialize class
        /// </summary>
        public UserClipHistory()
        {
            _clipHistory.Capacity = ListCapacity;
        }

        /// <summary>
        /// Return count of items on the list
        /// </summary>
        public int ItemCount
        {
            get { return _clipHistory.Count; }
        }

        /// <summary>
        /// Get item from the history list from the given index
        /// </summary>
        /// <param name="index">Index to access the data</param>
        /// <returns>Value from the list on the given index</returns>
        public string GetElement(int index)
        {
            if (index < 0 || index > _clipHistory.Count)
                return null;
            return _clipHistory.ElementAt(index);
        }

        /// <summary>
        /// Add new item first of the list or move existing same item to the first of the list
        /// </summary>
        /// <param name="content">Content to add</param>
        public void Add(string content)
        {
            if (!content.Equals(null))
            {
                int indexOfContent = _clipHistory.IndexOf(content);

                if (indexOfContent < 0)
                {
                    if (_clipHistory.Count == ListCapacity) // If the capacity if fully used then remove the olders item to create space for a new item
                    {
                        _clipHistory.RemoveAt(0);
                        _clipHistory.TrimExcess();
                    }
                    _clipHistory.Add(content);
                }
                // If item already exist on the list then remove it
                else
                {
                    _clipHistory.RemoveAt(indexOfContent);
                    _clipHistory.TrimExcess();
                    _clipHistory.Add(content);
                }
            }
        }

        /// <summary>
        /// Remove content from the history item list
        /// </summary>
        /// <param name="content">Content to remove from the history item list</param>
        public void Remove(string content)
        {
            if (!content.Equals(null))
            {
                _clipHistory.Remove(content);
                _clipHistory.TrimExcess();
            }
        }

        /// <summary>
        /// Remove first items from the history list.
        /// </summary>
        /// <param name="count">Number of consecutive items to remove, including the starting index</param>
        public void RemoveRangeOldestItems(int count)
        {
            try
            {
                _clipHistory.RemoveRange(0, count);
            }
            catch (ArgumentException)
            {
                throw new IndexOutOfRangeException();
            }
            finally
            {
                _clipHistory.TrimExcess();
            }
        }

        /// <summary>
        /// Clear list from items
        /// </summary>
        public void Clear() => _clipHistory.Clear();

        #endregion
    }
}
