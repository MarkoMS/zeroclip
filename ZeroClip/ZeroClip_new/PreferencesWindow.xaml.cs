﻿using System;
using System.Windows;
using System.Windows.Controls;
using ZeroClip_new.Classes;

namespace ZeroClip_new
{
    /// <summary>
    /// Preferences window for ZeroClip main Window
    /// </summary>
    public partial class PreferencesWindow : Window
    {
        public event EventHandler CapacityChange;
        public event EventHandler AlwaysOnTopChange;
        public event EventHandler TimerIntervalChange;
        private readonly UserClipHistory _history; // main window history object reference


        /// <summary>
        /// Window and properties initialization
        /// </summary>
        /// <param name="history">Class that contains settings for the main window</param>
        public PreferencesWindow(UserClipHistory history)
        {
            _history = history;
            InitializeComponent();
            InitializeHistory();
        }

        /// <summary>
        /// Update history items to this window (child) and set them into depedency properties
        /// </summary>
        private void InitializeHistory()
        {
            UpdateInterval = _history.UpdateInterval;
            ListCapacity = _history.ListCapacity;
            AlwaysOnTop = _history.AlwaysOnTop;
        }

        #region Depencency properties

        /// <summary>
        /// UpdateInterval property
        /// </summary>
        public int UpdateInterval
        {
            get { return (int)GetValue(UpdateIntervalProperty); }
            set
            {
                SetValue(UpdateIntervalProperty, value);
                _history.UpdateInterval = value;
            }
        }

        /// <summary>
        /// AlwaysOnTop property
        /// </summary>
        public bool AlwaysOnTop
        {
            get { return (bool)GetValue(AlwaysOnTopProperty); }
            set
            {
                SetValue(AlwaysOnTopProperty, value);
                _history.AlwaysOnTop = value;
            }
        }

        /// <summary>
        /// ListCapacity property
        /// </summary>
        public int ListCapacity
        {
            get { return (int)GetValue(ListCapacityProperty); }
            set
            {
                SetValue(ListCapacityProperty, value);
                _history.ListCapacity = value;
            }
        }
        /// Depedency property of UpdateInterval
        public static readonly DependencyProperty UpdateIntervalProperty = DependencyProperty.Register("UpdateInterval", typeof(int), typeof(PreferencesWindow));

        /// Depedency property of ListCapacity
        public static readonly DependencyProperty ListCapacityProperty = DependencyProperty.Register("ListCapacity", typeof(int), typeof(PreferencesWindow));

        /// Depedency property of AlwaysOnTop
        public static readonly DependencyProperty AlwaysOnTopProperty = DependencyProperty.Register("AlwaysOnTop", typeof(bool), typeof(PreferencesWindow));

        #endregion

        #region Event handling

        /// <summary>
        /// Interval value lost focus
        /// </summary>
        /// <param name="sender">Interval value field</param>
        /// <param name="e">Lost focus event</param>
        private void TextboxIntervalTimer_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox cliked = (TextBox)sender;

            if (Int32.TryParse(cliked.Text, out int intValue))
            {
                TimerIntervalChange.Invoke(intValue, EventArgs.Empty);
            }

        }

        /// <summary>
        /// Capacity value lost focus
        /// </summary>
        /// <param name="sender">Capacity value field</param>
        /// <param name="e">Lost focus event</param>
        private void TextboxHistoryCapacity_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox cliked = (TextBox)sender;

            if (Int32.TryParse(cliked.Text, out int capacity))
            {
                CapacityChange.Invoke(capacity, EventArgs.Empty); //notify main window of change
            }
        }

        /// <summary>
        /// Always on top value lost focus
        /// </summary>
        /// <param name="sender">Always on top value field</param>
        /// <param name="e">Lost focus event</param>
        private void CheckboxOnTop_LostFocus(object sender, RoutedEventArgs e)
        {
            CheckBox tickBox = (CheckBox)sender;
            bool isChecked = (bool)tickBox.IsChecked;
            AlwaysOnTopChange.Invoke(isChecked, EventArgs.Empty); //notify main window of change
        }

        #endregion
 
    }
}
